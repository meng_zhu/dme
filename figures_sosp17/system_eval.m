colormap copper;

FZ = 5;

subplot(4,1,1);

% energy

Default    = [ 1 1 1 1 1 1 1 1 1 1 1 1 1];
Bg_delay = [ 0.99 0.98 0.94 0.94 0.97 0.91 0.92 0.92 0.98 0.99 0.85 0.95 0.95 ];
Label_delay = [ 0.80 0.93 0.88 0.71 0.91 0.84 0.87 0.66 0.85 0.85 0.81 0.79 0.82];
system_delay = [ 0.78 0.9 0.87 0.68 0 0 0 0 0 0 0 0 0 ];

Y = Default' * [1 0 0 0] + Bg_delay' * [0 1 0 0] + system_delay' * [0 0 1 0] + Label_delay' * [0 0 0 1];
h = bar(Y, 0.6, 'group');
legend(h, ['Default' char(10) 'system'], ['Android process importance based scheduling' char(10) '(treating system task as interactive)'], ['Android process importance based scheduling' char(10) '(treating system task as background)'], ['Interactivity-aware' char(10) 'scheduling']);
set(legend, 'Position',[0.5 0.96 0.01 0.01], 'Orientation','horizontal');
axis([0 13.5 0 1]);
set(gca, 'FontSize', FZ);
%set(gca, 'XTick', 1:1:4);
set(gca, 'XTickLabel', {'1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', 'Average'});
set(gca, 'YTick', [0.6 0.7 0.8 0.9 1.0]);
ax = gca;
ax.GridLineStyle = '--';
set(gca, 'YGrid', 'on');
ylabel('Normalized energy');
box off;
title('(A) Total active energy consumption');

subplot(4,1,2);

%interactivity 

Default    = [ 1 1 1 1 1 1 1 1 1 1 1 1 1];
Bg_delay = [ 1.00 0.98 0.99 1.00 0.98 0.99 0.99 0.95 0.95 0.96 0.94 0.86 0.97 ];
Label_delay = [ 1.08 0.94 0.98 1.07 1.03 1.03 1.07 1.05 1.06 1.05 1.04 1.02 1.04 ];
system_delay = [ 1.03 0.97 0.99 1.13 0 0 0 0 0 0 0 0 0 ];

Y = Default' * [1 0 0 0] + Bg_delay' * [0 1 0 0] + system_delay' * [0 0 1 0] + Label_delay' * [0 0 0 1];
h = bar(Y, 0.6, 'group');
axis([0 13.5 0 1.2]);
set(gca, 'FontSize', FZ);
%set(gca, 'XTick', 1:1:4);
set(gca, 'XTickLabel', {'1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', 'Average'});
set(gca, 'YTick', [1 1.1 1.2]);
ax = gca;
ax.GridLineStyle = '--';
set(gca, 'YGrid', 'on');
ylabel('Normalized latency');
title('(B) Interactive task response latency');
box off;

fname = 'system_eval';
print('-depsc', fname);
cmd = sprintf('gs -q -sDEVICE=pdfwrite -dEPSCrop -sOutputFile=%s.pdf -dBATCH -dNOPAUSE %s.eps', fname, fname);
system(cmd);

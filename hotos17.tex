\documentclass[10pt,twocolumn]{article}

\usepackage{times}
\usepackage{epsfig}
\usepackage{cite}
%\usepackage{subfigure}
\usepackage{subcaption}
\usepackage{xspace}
\usepackage[shortcuts]{extdash}
\usepackage{caption}
\usepackage{url}
\usepackage{color}

\def\baselinestretch{1.00}

% suppress page numbers.
%\pagestyle{empty}

%set dimensions of columns, gap between columns, and space between paragraphs
\setlength{\textheight}{9.0in}
\setlength{\textwidth}{6.5in}
\setlength{\columnsep}{0.25in}
%\setlength{\footskip}{0.0in}
\setlength{\topmargin}{0.0in}
\setlength{\headheight}{0.0in}
\setlength{\headsep}{0.0in}
\setlength{\oddsidemargin}{0.0in}
\setlength{\evensidemargin}{0.0in}
%%\setlength{\parindent}{0pc}
%%\setlength{\parskip}{\baselineskip}

% started out with art10.sty and modified params to conform to IEEE format
% further mods to conform to Usenix standard

\makeatletter

%as Latex considers descenders in its calculation of interline spacing,
%to get 12 point spacing for normalsize text, must set it to 10 points
\def\@normalsize{\@setsize\normalsize{12pt}\xpt\@xpt
\abovedisplayskip 10pt plus2pt minus5pt\belowdisplayskip \abovedisplayskip
\abovedisplayshortskip \z@ plus3pt\belowdisplayshortskip 6pt plus3pt
minus3pt\let\@listi\@listI}

%need a 12 pt font size for subsection and abstract headings
\def\subsize{\@setsize\subsize{12pt}\xipt\@xipt}

%make section titles bold and 12 point, 2 blank lines before, 1 after
%\def\section{\@startsection {section}{1}{\z@}{24pt plus 2pt minus 2pt}
%{12pt plus 2pt minus 2pt}{\large\bf}}
\def\section{\@startsection {section}{1}{\z@}{16pt}{9pt}{\large\bf}}

%make subsection titles bold and 11 point, 1 blank line before, 1 after
%\def\subsection{\@startsection {subsection}{2}{\z@}{12pt plus 2pt minus 2pt}
%{12pt plus 2pt minus 2pt}{\subsize\bf}}
\def\subsection{\@startsection {subsection}{2}{\z@}{9pt}{9pt}{\subsize\bf}}

% less space before level-4 paragraphs
\def\paragraph{\@startsection
     {paragraph}{4}{\z@}{1.25ex plus1ex minus.2ex}{-1em}{\reset@font
     \normalsize\bf}}

\makeatother

\newcommand{\notered}[1]{\textcolor{red}{[{\bf #1}]}}
%\newcommand{\notered}[1]{}

\begin{document}

% Seem to be necessary for letter-size (instead of A4-size) PDF
\setlength{\pdfpageheight}{11in}
\setlength{\pdfpagewidth}{8.5in}

\title{\vspace*{-0.3in}
\Large\bf Interactivity As A First-Class Context for Mobile OSes}

\author{
Meng Zhu \\ \emph{University of Rochester}
\and
Kai Shen \\ \emph{Google, Inc.}
}

\date{}
\maketitle

\begin{abstract}
This paper proposes a first-class execution context for mobile operating systems
that reflects the criticality of current execution on user interactivity.
The interactive context will enable energy and performance optimizations in many
OS functions including CPU scheduling, I/O prioritization, and processor\,/\,device
power state control.  Mobile OSes like Android\,/\,Linux possess a wealth of
information on the initiation and propagation of user interactivity sessions.
%including semantic system calls, kernel block\,/\,wakeup relations, Android UI
%and messaging framework.
However, OS-level tracking of interactivity is
imprecise---e.g., user-space synchronization and batched event processing lead
to OS-invisible or ambiguous context propagation.  We propose that resource
scheduling and power state control
should directly manage uncertainty of the interactivity context.  We further
propose that the OS should disambiguate the interactivity context at each exit
(to user space or a device dispatch) so that executions outside the OS can be
properly managed.  A prototype interactivity tracker, CPU scheduler, and core
frequency adapter on a quad-core smartphone demonstrates the potentials for
substantial energy saving and improved user response.
%Our prototype
%interactivity tracker, CPU scheduler, and core frequency adapter on a quad-core
%smartphone shows up to 23\% energy reduction at negligible loss (or
%even improvement) of user interactivity.
\end{abstract}

\section{Introduction}

% motivation
Mobile processors and devices are increasingly heterogeneous and dynamically
configurable. For instance, latest Snapdragon mobile CPUs allow per-core, dynamic
frequency and voltage control that enables fine-grained (below the millisecond
scale) adjustment on energy-performance trade-offs~\cite{snapdragon-percore}.
%Such system is capable of providing agile differential control over 
%tasks with different priorities.
At the same time, mobile systems run a variety of versatile, concurrent tasks
with unequal importance toward user interactivity.  Typically, a
smartphone user focuses on one task at a time---e.g., a user browsing the web
is only concerned with the rendering speed and scroll smoothness of the current
webpage.  The same browser application may contain other webpages loading in
background tabs and user data like new bookmarks or browsing history might be
syncing to the cloud.  Furthermore, system activities like GPS tracker and
software updates may be running simultaneously.  A substantial amount of
execution in such a system may be delayed without affecting user interactivity.

% introduction of interactivity context
We introduce a new interactivity context for mobile OSes.  Specifically, the
OS labels each unit of execution---a period of CPU execution between context
switches, a dispatch to an I/O device---as \emph{interactive} or \emph{background}
depending on its perceived importance of affecting user response latency. 
Awareness of interactivity will enable optimizations in a variety of OS functions.
For instance, CPU~\cite{song-hotmobile-2014, zhu-atc-2016}, network~\cite{balasubramanian-imc-2009},
and I/O~\cite{papathan-usenix-2004,jeong-fast-2015} resources can be properly
prioritized for performance, or dynamically configured (e.g., adjusting the
power state) for energy efficiency at no loss of user-perceived performance.
%Work can be delayed and shaped (along with
%device sleeps and CPU configuration adaptation) for energy efficiency.
We believe its broad implication and importance for mobile resource
management makes the interactivity context a first-class OS construct.

% challenges
A user session may involve executions on a number of application processes
and OS tasks.  Also notably, a system process may perform work on behalf of
interactive user sessions and background jobs alternately. 
Thus tracking the intricate inter-dependencies between different
tasks on the fly is necessary to maintain the interactive context.
Fortunately, a mobile OS with unified UI, runtime, and kernel framework
already possesses a variety of information to infer the initiation and
propagation of a user interaction session.  In Android\,/\,Linux, for
example, a UI session starts on well-defined events like touch-screen
actions.  Dependency executions of a UI session can be tracked by monitoring
control\,/\,data flow system calls (futex, epoll, sockets), kernel-level
block and wakeup relations, and Android binder (inter-process) and message
queue (intra-process) communications.

% best-effort challenge
However, OS-level interactivity tracking suffers from inherent inaccuracies.
In particular, some synchronizations between UI stages are preformed at the
user space (particularly when kernel block-wakeup support is not needed)
that are invisible to the OS\@.  Additionally, event dispatchers in a mobile OS may return a batch
of events (belonging to both interactive and non-interactive contexts) for
efficiency but the context of subsequent execution becomes ambiguous.
We propose two OS design principles to address these challenges:
\begin{itemize}
\item A new \emph{uncertain} context covers executions whose importance for
	user interactivity cannot be confidently recognized.  They should
	be properly scheduled to minimize priority inversion.  Their device power
	state will depend on the relative importance between UI latency and
	energy efficiency.
\item The OS should actively disambiguate the interactivity context to
	enable proper resource management.  Specifically, the return of a
	system call or the dispatch of an I/O operation may only carry a clear
	interactivity (or background) context.  Therefore event batching or
	I/O merging can only be performed on a per-context basis.
\end{itemize}

%related work

%Primitive forms of interactivity context already exist in the OS.  Examples 
%include real-time scheduling task labels, Android foreground process labels~\cite{android-process-lifecycle},
%and synchronous I/O context.  We fundamentally go far beyond these examples.

%\cite{song-hotmobile-2014} leverages the mobile phone UI centric model to improve energy efficiency: monitor the UI thread activity (and its dependencies) to determine when UI transaction ends and lower the CPU configuration. Note no context is maintained, the whole system is either in transaction or not.

AppInsight~\cite{ravindranath-osdi-2012} and Timecard~\cite{ravindranath-sosp-2013}
instrumented mobile app binaries to identify user transaction critical path and
perform response-aware server adaptation.  Their approach depends on the use
of a specific application development framework (Microsoft Silverlight)
which eases the tracking of dependencies among application methods and callbacks.
In the server domain, Resource Container~\cite{banga-osdi-1999} and
Magpie~\cite{barham-osdi-2004} tracked individual requests using application 
binding and developer-provided event semantics.  In this paper, we argue that
the user interactivity context can be transparently maintained in a mobile OS 
given the unified UI, runtime, and kernel framework, and that the interactivity
context can effectively improve energy efficiency and performance through managed uncertainty
and context disambiguation.

%Our system lives transparently within OS
%(Android/Linux) runtime that doesn't restrict how apps are programmed?
%In addition, their critical path construction only covers application
%execution segments while missing system/kernel executions and cross-application
%dependencies.

%\notered{Related work of context tracking beyond mobile?  Servers?  Network systems?}

\section{Benefits and Feasibility}

\paragraph{Versatile, Concurrent Mobile Workload}

%\notered{I like the emphasis on new application trends.  We can possibly 
%highlight that the user interactivity does not match well with existing 
%system contexts (to differentiate/separate executions) like process/thread
%context and synchronous/asynchronous I/O contexts.  Can we have some
%specific data on these points?}

As smartphones become more powerful, they are increasingly used
for a variety of purposes. Applications grow in sophistication and more
of them are running at the same time. Yet users' attention at a given
moment remains the same. Typically, smartphone users focus on one task at a
time. % the one they just interacted with.
Residual executions of the previous user interaction
session and other background tasks
do not relate to the current user interaction and thus have little
direct impact on user-perceived quality-of-service.

%In this paper, we categorize smartphone
%tasks into two kinds: (1) interactive tasks and its dependencies
%triggered by the most recent user interactions that probably
%relate to the user-perceived experience whose performance is
%of paramount importance; (2) background tasks
%that do not relate to user interaction and 
%thus have loose quality-of-service requirements.
%There are ample background tasks in today's smartphone workloads,
%both in multi-tasking as well as single application scenarios.

Multi-tasking on smartphones is increasingly common. Download or upload
operations to sync data with cloud services, post to social media
websites and install\,/\,update applications are typical smartphone usages.
In addition, as smartphones get ``smarter'', they are taking every
opportunity to sense and understand the current context preemptively
in the background. GPS and motion sensors track user fitness activities;
virtual personal assistants like Siri analyze user usage patterns to
provide news and recommendations; other more creative ways
of sensing like eye tracking~\cite{sticky} are also emerging.
These tasks often do not have hard deadlines and thus could 
be optimized to avoid resource contention with interactivity or
minimize the energy cost. 

In our experiments, when launching the application Google Music on
the Nexus 5 smartphone, we find that at least 19\% of the executions
(in CPU cycles) can be delayed without increasing the user-perceived
latency. And when it co-runs with a background application installation task,
at least 36\% of the executions can be delayed without impacting
the user experience. This shows promising potential in applying
differential control in interactive and background tasks.

However, existing system abstractions such as process\,/\,thread,
Linux cgroup (a collection of processes with resource isolation), event dispatcher
(e.g. {\tt\small epoll\_wait()}), and
synchronous\,/\,asynchronous I/O do not match well
with the user interactive context.
These system constructs consolidate work to improve overall system
efficiency. Yet the whole-system metric does not necessarily relate
to user-perceived performance.
For example, interactive context
spans across a dynamic set of threads.
A single thread may frequently switch between interactive
and background contexts. This is especially common on Android where a
single thread may host several application components that serve both
foreground user interactions as well as
background operations~\cite{app-component}.

%For example, we find that 32\% of the executions
%in launching Google Music belongs to system services that are not
%of any application processes. They can potentially serve both interactive
%and non-interactive tasks.

\paragraph{Heterogeneous, Configurable Platforms}

%\notered{We may highlight the per-core DVFS's ability of enabling more dynamic 
%energy/performance adaptation (in contrast to big.LITTLE platforms where 
%workload has to be hard partitioned to different CPU clusters).}

Smartphone users continually desire new and powerful 
features as well as longer battery life. 
Facing such challenges, modern smartphones are equipped with
heterogeneous, dynamically configurable hardware with 
different performance-energy trade-offs.

Mobile multicore processors are the best example.
They have very sophisticated power states which can be dynamically adjusted to
adapt to different workloads. Heterogeneous chips 
with cores of different micro-architectures (e.g., ARM big-LITTLE) 
are used to provide power-proportionality in a wide
power-performance range.
In addition, recent mobile CPUs allow per-core
frequency and voltage adjustment. For example,
each core's frequency of the Snapdragon 800 quad-core processor can be
independently adjusted between 300\,MHz and 2.3\,GHz with
a power difference of 1.06\,W\@ for each core.
In comparison, the whole Nexus 5 smartphone's active idle power
consumption is 0.33\,W.
This enables nimble 
energy-performance trade-offs (less than 50$\mu$Secs adjustment latency).
On big-LITTLE platforms, threads of different priorities need to
be migrated between different clusters for performance and power scaling.
A degree of thread persistence on
the cluster is required to amortize the migration cost.
Per-core adjustment, on the other hand, do not rely on thread migration
which is particularly suitable for providing interactivity based differential
control where a thread may get in and out of different contexts very frequently.

Smartphone radio is another example. At high power state,
it provides a dedicated channel to the device and ensures high throughput
and low delay for transmissions. But it is inefficient due to high tail
energy overhead~\cite{balasubramanian-imc-2009}.
While at low power states, a shared channel with lower
transmission rate is used to minimize the energy cost.
%An energy-aware system could choose to transmit low
%priority data at low power state or piggyback it
%over high priority transmissions at high power state to amortize
%the tail energy overhead.  Similar techniques have been proposed for GPS and
%other sensors~\cite{lane-sensys-2013}.

%With the slow progress on battery technologies and
%ever increasing size restrictions of hand-held devices, we expect
%more hardware components with wider performance-energy trade-offs
%to emerge.

%Smartphone tasks that can tolerate delays could
%be manipulated to achieve higher energy efficiency. 

%\notered{I thought about mobile Flash but didn't figure out anything that
%fits into our narrative.}

\paragraph{Exposed Application Semantics}

%\notered{Think of what clear, strong message you can advocate here and focus on it.
%Also make Figure~\ref{fig:ui_trans} to focus on the message.}

%\notered{Can we argue that mobile applications are often developed using
%high-level framework with standard utilities for concurrency and
%asynchronous callbacks? Such standardization makes interactivity tracking
%much more feasible than traditional OS-level tracking of context
%propagation.  Does this get us too close to AppInsight~\cite{ravindranath-osdi-2012}?}
%
%\notered{Try to be explicit and make powerful argument?  For instance,
%rather than just saying mobile OS offers more information on context
%propagation, explain that prior kernel-based context propagation
%maintenance primarily suffers from missing XXX and YYY, and then follow
%up that XXX and YYY are well standardized in mobile development framework
%and they can be well captured by the mobile OS runtime.}

Thanks to the unified mobile application and runtime framework,
much of application semantics are exposed to the system, making
it feasible to track the interactivity context transparently with fair accuracy.
%In addition to traditional kernel-level information
%such as semantic system calls, mobile 
%OSes like Android possess a variety of extra information.

The well-defined application and language APIs make it much
easier to track task dependencies.
Shared memory based intra-process communications are invisible
to a generic kernel system. They are primary challenges
for application-transparent dependency tracking. Fortunately, these are well
standardized in the mobile application development framework.
On Android, for instance, they can be captured through
light instrumentation in key system facilities such as thread message queue 
and the Java concurrent library.

Smartphone also has unified UI framework which gives the system intimate
knowledge about application runtime status.
%On Android, only one UI thread
%is allowed to directly interact with the user and update
%screen contents which gives clear signals of initialization
%and completion of interactive sessions.
%It also gives strong hints about task context.
For example, application processes on Android are placed into an
importance hierarchy for memory management
based on user visibility~\cite{android-process-lifecycle}.
This could be utilized to cross-check our context propagation
to guard against worst-case tracking errors
(e.g., labeling user-visible foreground thread as non-interactive). 

Figure~\ref{fig:ui_trans} illustrates a representative
(but simplified) user interaction session
on Android. A user touch event wakes up
the application UI thread which belongs to the foreground
process group. It then submits an asynchronous callback to the worker
thread which in turn makes a request to the
system server daemon to query the device.
Thanks to the standardized Android facilities such as
binder (inter-process) and
message queue (intra-process) communications,
important control and data flow between different entities are well exposed,
beckoning system-level context tracking.

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{figures/ui_transaction.pdf}
  \caption{An illustrative example of a user interaction session.
          The user touches a button to query the device status.
          Important data and control flows between threads
          are marked with arrows. Darkened portions of a
          thread timeline indicate active executions
          (while the rest represent blocking waits).}
  \label{fig:ui_trans}
\end{figure}

\section{OS Design Issues}
\label{sec:os}

At a high level, supporting interactivity context in a mobile OS involves
the capture of UI session's initiation and propagation in the system,
as well as interactivity-driven resource management.
%We propose the design of a transparent OS-level facility
%that tracks execution context propagation in Android\,/\,Linux with
%no assumption on application programming model or externally supplied
%hints. Our approach is best-effort in that it recognizes the inherent
%inaccuracies of OS-level interactive tracking and minimizes the
%impact of such inaccuracies on making resource management decisions.
An interactive context starts when events from user input devices
(typically touchscreen interrupts) wake up the UI thread of the current
foreground application.  A mobile OS can capture these instances by
recognizing the foreground application (through Android process importance
hierarchy~\cite{android-process-lifecycle}) and monitoring relevant
kernel event interface {\tt\small epoll\_wait()}.
Each time, two execution contexts are initiated---the interactive
context contains the foreground threads (the UI thread and its siblings)
and the background context includes all remaining threads.

After the initiation, execution context may propagate through
additional threads and processes.
We capture such propagations through OS-visible data and control flows.
Specifically, we identify the following kernel primitives to propagate
execution contexts---process (or thread) forking, signals, Unix domain sockets
and semantic system calls including {\tt\small futex()} and {\tt\small epoll\_wait()}.
We also rely on key Android facilities such as binder and thread message queue
to catch important inter-process
as well as shared memory based intra-process communications.
In each case, successors (child in forking, epoll event processor,
message receiver in communication
primitives such as binder and message queue and wakee in kernel block\,/\,wakeup
relations) inherent the execution contexts of the predecessors 
(forking parent, event producer for epoll, message
sender and waker in corresponding cases).

Awareness of interactivity enables a variety of optimizations in OS resource
management.  Schedulers for CPU, I/O, network, and other devices
can prioritize interactive executions over background work.
Processors and devices may offer multiple power states (e.g., core frequency levels)
where higher power states deliver better performance at higher energy cost per
unit of work.  In such cases
lower power state should be adopted during background work to save
energy at no loss of user-perceived performance.  The OS may also delay
and consolidate background work in coarse-grained bursts which consequently
create sufficient idle periods for low-power CPU\,/\,device sleeps~\cite{papathan-usenix-2004,balasubramanian-imc-2009,zhu-atc-2016}.

In the I/O domain, only a synchronous I/O dispatched under
an interactive context is considered interactive.
A synchronous I/O is one whose completion is waited on by the caller process
and all synchronous I/Os are presumed by traditional OS I/O schedulers to be 
performance-critical.  However, the
user-perceived performance is what ultimately matters and fast response to the
caller process does not necessarily mean fast user responses.
Our re-thinking of synchronous I/O's role in performance-criticality is 
reminiscent of Nightingale et al.~\cite{nightingale-tocs-2008}'s 
relaxation of synchronous I/O's durability semantics according to end user
visibility.

\paragraph{Imprecise Context Maintenance}
OS-level dependency tracking suffers from inherent inaccuracies.
In particular, an interactive context may propagate through OS-invisible
synchronization.
For instance, a thread may signal another waiting thread through a monitor
(including a mutex lock and condition variables).  Much of a synchronization
operation can be implemented in the user space except when the waiter has to
block (e.g., yielding the CPU through a {\small\tt futex} wait in Linux)
while waiting for the synchronization signal.
If such blocking is unnecessary (e.g., when the waiter can proceed
immediately since the waited-for signal has already occurred), the
entire synchronization would present no visibility to the OS\@.
A limited (and expensive) solution is to recognize synchronization data structures
in advance and invalidate relevant page accesses for OS traps~\cite{chanda-eurosys-2007}.

Event-driven processing is popular in UI-oriented mobile systems.
It decomposes application work into non-blocking handlers or callbacks
triggered by specific events.  Event polling and dispatch are usually handled
by OS primitives such as {\tt\small select()} and {\tt\small epoll\_wait()}.
For efficiency, an event dispatch call may return a batch of ready events for
user space processing.  However, events in a returned batch may
signal a mixture of interactive as well as background work, and thus the context of
subsequent user-space execution becomes ambiguous.  We propose two OS design
principles to address these problems.

\paragraph{Resource Management with Uncertainty}
%Besides the interactive and background contexts, 
We introduce a new (third) 
\emph{uncertain} context that covers executions whose criticality for user
responses cannot be confidently recognized.  We highlight several specific cases.
First, when the {\tt\small epoll\_wait()} system call returns a batch of events
with mixed interactive and background contexts, the interactivity status of
subsequent executions is uncertain. 
Similarly, when the OS merges data from interactive as well as background
contexts into a single I/O dispatch, its interactivity status is uncertain.
Furthermore, uncertainty arises when interactivity inferred from different
information sources sometimes conflict with each other.  For example, a
thread marked by Android process importance hierarchy as foreground may inherit
background context propagation from another background thread.

%When such conflicts arise, we expose such uncertainty
%to OS resource management functions which make decisions based on specific workload
%conditions and performance goals.

Our general goal of resource scheduling with uncertainty is to make scheduling
performance (application response latency and energy saving) proportionate to the
completeness and perfection of interactivity context tracking.  In particular we
try to minimize the possibility of priority inversion---an interactive execution
is scheduled behind background work.  Our resource schedulers first run work of
interactive context, then work of uncertain context, and last work of background
context.  Under this approach, assuming that interactive and background contexts
are correct, priority inversion may only happen within the uncertain context.
When energy-saving power states are available, 
%interactive context should clearly
%use a high power state and background work should run at a low power state.
the power state for running work of uncertain context should be determined
according to the relative importance between UI latency and energy efficiency---%
using a high power state if response latency is more important,
and using a low power state otherwise.

\paragraph{Context Disambiguation}

The OS can and should actively disambiguate the interactivity context at each
OS exit to user space or a device I/O dispatch.  This will allow proper
resource management of subsequent execution, and enable proper tracking of
further context propagation.  For batched event dispatches, we require that
each dispatched event batch should carry the same interactive (or background)
context.  Specifically for the {\tt\small epoll\_wait()} system call, if a
mixture of events from interactive and background contexts are available,
we first return the interactive events in a batch.  We only return the
background events when no interactive event is available.

Context disambiguation also applies to I/O submissions.
I/O merging is a common technique to improve I/O efficiency.
When interactive and background I/O are merged, the resulted device 
operation would have an uncertain context and therefore cannot benefit from
interactivity-driven optimizations.  Since merged I/O operations are from
nearby device addresses and likely related in semantics, they normally 
belong to the same interactive (or background) context.  
The most common situation for mixed-context merging is probably file system journaling %(or logged-structured file systems)
when writes are merged 
simply based on nearby submission time.  Our I/O context disambiguation
requires that an I/O operation is tagged with interactive (or background)
context at the time of submission.  The context tag propagates to
derived I/O for metadata and journaling.  At the time of I/O merging
or assembling journal I/O for a transaction, we ensure that
I/O is only merged on a per-context basis.

Context disambiguation reduces the event dispatch batch size 
(more system calls) and the I/O merging capability (more I/O dispatches),
resulting in degraded efficiency.  Our experience suggests that reduced
event dispatch batch on a mobile system does not exhibit a noticeable
performance impact.  However, increased write traffic to mobile Flash leads to
non-negligible performance risks.  Each context disambiguation technique should
be weighed against the alternative of full batching\,/\,merging with
uncertain interactivity.

\section{Prototyping and Results}

%Emerging mobile processors are capable of doing per-core frequency
%and voltage adjustment to provide fine-grained performance-energy trade-offs
%with minimal overhead. Unfortunately,
%current mobile systems
%do not take full advantage of this feature---the common
%practice is to blindly raise the CPU frequency along with the
%overall system load without
%considering the nature of running tasks.
%Our interactivity-aware system, on the other hand, would enable fine-grained
%differential control to make most use of this powerful feature.

We prototyped our context tracking facility on Nexus
5 smartphone running Android 4.4. Each core's frequency of its quad-core processor
can be independently adjusted between
300\,MHz and 2.3\,GHz.
Current mobile systems do not fully utilize this
fine-grained performance-energy adjustment feature---the common
practice is to set the core frequency strictly based on the
overall system load with no consideration of the varying importance of the load.
In contrast, our system utilizes fine-grained interactivity context to prioritize 
interactive work and adjust CPU core frequency accordingly.

%To track the execution context, we embed a context flag (interactive,
%background or uncertain) in each {\tt\small task\_struct}. This flag is then
%propagated through various dependency events discussed in Section~\ref{sec:os}.

Our CPU scheduler picks tasks in each core's run queue in three descending levels of
context priorities: interactive, uncertain and background. In addition to improving system
interactivity and minimizing the possibility of priority inversion, this also
consolidates tasks of the same context and divides CPU executions into alternating
phases, facilitating further differentiated frequency control for energy optimizations.

Core frequency is controlled by the CPU governor based on
the system load.
To improve energy efficiency without impacting interactivity,
we modify the governor to ignore the load of background tasks---higher core
frequency only depends on more interactive load.
Therefore after a transition from interactive to background phase,
the core frequency gradually drops to the lowest, resulting in great energy efficiency.
When interactive tasks wake up, the governor 
responds quickly to satisfy their performance needs.

We select two interactive workloads: \emph{AppLaunch} launches the
Google Music application and \emph{Bbench} automatically loads and renders locally
cached websites~\cite{gutierrez2011full}. We also choose to co-run a
common background task \emph{AppInstall}---to install the Facebook application---with
each interactive workload.
Results in Figure~\ref{fig:sched_eval} show the benefits of
interactivity-aware CPU scheduling and core frequency management.
It saves up to 23\% energy with little user-perceived 
performance impact. In the case of \emph{Bbench}, it even improves the
performance by 8\% thanks to the scheduler prioritization
on interactive tasks.

\begin{figure}[h]
  \centering
  \includegraphics[width=\columnwidth]{figures/sched_eval.pdf}
  \caption{Interactive task response latency and system energy cost under baseline 
	  and interactivity context-aware OS\@. Both the latency and energy cost
	  are normalized with the corresponding interactive workload running alone
	  under the baseline setting.}
  \label{fig:sched_eval}
\end{figure}

\section{Conclusion and Discussions}

This paper proposes a first-class mobile system context that reflects the
criticality of current execution on user interactivity.  We argue that a mobile
OS possesses sufficient information to capture the initiation and propagation of
UI sessions.  We further demonstrate that the interactivity context may
substantially improve the energy efficiency at no loss of quality-of-service,
while implicating the design of a variety of OS functions including CPU scheduling,
power state control, event dispatching, as well as I/O merging and
prioritization.

Aside from direct control and data flows, the interactivity dependency may
manifest in more subtle ways.
For instance, drowsy power management~\cite{lentz-sosp-2015} monitors device open,
close, and read\,/\,write accesses to model a device state machine and infer
task-device dependencies.
In another example, quasi-asynchronous I/O~\cite{jeong-fast-2015} is recognized that
file system functions sometimes depend on the completion of asynchronous I/O
(that was issued earlier on blocks needed by later synchronous functions).
They call for more comprehensive dependency tracking in the system, or exposing
the uncertain context when precise tracking is impossible or too 
expensive.

Given the adoption of multicore processors on mobile devices, the long-recognized
multicore resource contention issue~\cite{tam-eurosys-2007,fedorova-pact-2007}
becomes relevant.  The interactivity context may help mitigate such contention
to improve user performance.  For instance, when a CPU core finds that its own
work queue only contains background tasks while a resource-contending sibling
core is running interactive work, it may temporarily suspend execution or reduce
its speed (lower core frequency).  Such a contender throttling mechanism is not
work conserving but it should improve user-perceived performance given
correct interactivity context.

\bibliographystyle{abbrv}
\bibliography{paper}

\end{document}

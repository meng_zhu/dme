colormap copper;

FZ = 5;

subplot(4,1,1);

% energy

Default    = [ 1 1 1 1 1 1 1 1 1 1 1 1 1];
normal_bind = [ 0.84 0.84 0.90 0.82 0.84 0.92 0.87 0.78 0.92 0.91 0.91 0.86 0.87];
early_bind = [ 0.85 0.87 0.91 0.80 0.84 0.91 0.88 0.77 0.91 0.87 0.91 0.88 0.87];

Y = Default' * [1 0 0] + normal_bind' * [0 1 0] + early_bind' * [0 0 1];
h = bar(Y, 0.6, 'group');
legend(h, ['Default' char(10) 'system'], ['Interactivity-aware scheduling' char(10) 'with conventional context binding'], ['Interactivity-aware scheduling' char(10) 'with early context binding']);

set(legend, 'Position',[0.5 0.97 0 0], 'Orientation','horizontal');
axis([0 13.5 0 1]);
set(gca, 'FontSize', FZ);
%set(gca, 'XTick', 1:1:4);
set(gca, 'XTickLabel', {'1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', 'Geometric Mean'});
set(gca, 'YTick', [0.6 0.7 0.8 0.9 1.0]);
ax = gca;
ax.GridLineStyle = '--';
set(gca, 'YGrid', 'on');
ylabel('Normalized energy');
box off;
title('(A) Total active energy consumption');

subplot(4,1,2);

%interactivity 

Default    = [ 1 1 1 1 1 1 1 1 1 1 1 1 1];
normal_bind = [ 1.02 1.00 1.01 1.06 1.00 0.96 1.02 1.11 0.98 0.96 1.02 1.13 1.02];
early_bind = [ 1.03 1.01 1.02 1.02 0.99 0.92 1.02 1.05 1.00 0.93 1.02 1.06 1.01];

Y = Default' * [1 0 0] + normal_bind' * [0 1 0] + early_bind' * [0 0 1];
h = bar(Y, 0.6, 'group');
axis([0 13.5 0 1.2]);
set(gca, 'FontSize', FZ);
%set(gca, 'XTick', 1:1:4);
set(gca, 'XTickLabel', {'1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', 'Geometric Mean'});
set(gca, 'YTick', [1 1.1]);
ax = gca;
ax.GridLineStyle = '--';
set(gca, 'YGrid', 'on');
ylabel('Normalized latency');
title('(B) Interactive task response latency');
box off;

fname = 'early_bind_eval';
print('-depsc', fname);
cmd = sprintf('gs -q -sDEVICE=pdfwrite -dEPSCrop -sOutputFile=%s.pdf -dBATCH -dNOPAUSE %s.eps', fname, fname);
system(cmd);

colormap copper;

FZ = 5;

subplot(4,2,5);

% energy

Default    = [ 1 1 1];
normal_bind = [ 0.82 0.78 0.86];
early_bind = [ 0.80 0.77 0.88];

Y = Default' * [1 0 0] + normal_bind' * [0 1 0] + early_bind' * [0 0 1];
h = bar(Y, 0.6, 'group');
legend(h, ['Default system'], ['Conventional context binding based interactivity-aware scheduling'], ['Early context binding based interactivity-aware scheduling']);

set(legend, 'Position',[0.3 0.54 0 0]);
%set(legend, 'Location', 'northoutside');
axis([0 4 0 1]);
set(gca, 'FontSize', FZ);
set(gca, 'XTick', 1:1:3);
set(gca, 'XTickLabel', {'4', '8', '12'});
set(gca, 'YTick', [0.6 0.7 0.8 0.9 1.0]);
ax = gca;
ax.GridLineStyle = '--';
set(gca, 'YGrid', 'on');
ylabel('Normalized energy');
box off;
title('(A) Total active energy consumption');

subplot(4,2,7);

%interactivity 

Default    = [ 1 1 1];
normal_bind = [ 1.06 1.11 1.13];
early_bind = [ 1.02  1.05 1.06];

Y = Default' * [1 0 0] + normal_bind' * [0 1 0] + early_bind' * [0 0 1];
h = bar(Y, 0.6, 'group');
axis([0 4 0 1.2]);
set(gca, 'FontSize', FZ);
set(gca, 'XTick', 1:1:3);
set(gca, 'XTickLabel', {'4', '8', '12'});
set(gca, 'YTick', [1 1.1]);
ax = gca;
ax.GridLineStyle = '--';
set(gca, 'YGrid', 'on');
ylabel('Normalized latency');
title('(B) Interactive task response latency');
box off;

fname = 'early_bind_eval';
print('-depsc', fname);
cmd = sprintf('gs -q -sDEVICE=pdfwrite -dEPSCrop -sOutputFile=%s.pdf -dBATCH -dNOPAUSE %s.eps', fname, fname);
system(cmd);

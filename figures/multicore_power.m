colormap copper;

FZ = 6;

subplot(3,3,1);
%set(gca, 'FontSize', FZ);
power = [0.34 1.87 2.20 2.55 2.84];
h = bar(power, 0.4);
%colormap copper;
%ch = get(h,'Children');
%fvd = get(ch,'Faces');
%fvcd = get(ch,'FaceVertexCData');
%fvcd(fvd(2,:)) = 2;
%set(ch,'FaceVertexCData',fvcd);
axis([0 6 0 4.5]);
set(gca, 'XTick', 1:1:5, 'FontSize', FZ);
set(gca, 'XTickLabel', 0:1:4, 'FontSize', FZ);
xlabel('Number of active CPUs', 'FontSize', FZ);
ylabel('Power (Watts)');
box off;
title('(A) Tegra 3 based Nexus 7 tablet', 'FontSize', FZ);

%subplot(2,2,2);
%set(gca, 'FontSize', FZ);
%power = [0.051*4.2 0.12*4.2 0.15*4.2 0.178*4.2 0.210*4.2 0.49*4.2 0.629*4.2 0.787*4.2 0.93*4.2];
%h = bar(power, 0.4);
%ch = get(h,'Children');
%fvd = get(ch,'Faces');
%fvcd = get(ch,'FaceVertexCData');
%fvcd(fvd(2,:)) = 2;
%fvcd(fvd(6,:)) = 2;
%set(ch,'FaceVertexCData',fvcd)
%axis([0 10 0 4.5]);
%set(gca, 'XTick', [1 5 9]);
%set(gca, 'XTickLabel', {'0', '4', '8'});
%xlabel('Number of active CPUs');
%ylabel('Power (Watts)');
%box off;
%title('(B) Exynos 5422 based Galaxy S5');

subplot(3,3,2);
%set(gca, 'FontSize', FZ);
power = [0.048*4.2 0.1*4.2 0.122*4.2 0.144*4.2 0.166*4.2 0.413*4.2 0.540*4.2 0.68*4.2 0.82*4.2];
h = bar(power, 0.4);
%ch = get(h,'Children');
%fvd = get(ch,'Faces');
%fvcd = get(ch,'FaceVertexCData');
%fvcd(fvd(2,:)) = 2;
%fvcd(fvd(6,:)) = 2;
%set(ch,'FaceVertexCData',fvcd);
axis([0 10 0 4.5]);
set(gca, 'XTick', [1 5 9], 'FontSize', FZ);
set(gca, 'XTickLabel', {'0', '4', '8'}, 'FontSize', FZ);
xlabel('Number of active CPUs', 'FontSize', FZ);
ylabel('Power (Watts)', 'FontSize', FZ);
box off;
title('(B) Kirin 925 based Huawei Mate 7 smartphone', 'FontSize', FZ);

subplot(3,3,3);
set(gca, 'FontSize', FZ);
power = [0.33, 1.39, 2.45, 3.51, 4.57];
h = bar(power, 0.4);
%ch = get(h,'Children');
%fvd = get(ch,'Faces');
%fvcd = get(ch,'FaceVertexCData');
%fvcd(fvd(2,:)) = 2;
%set(ch,'FaceVertexCData',fvcd);
axis([0 6 0 4.5]);
set(gca, 'XTick', [1:1:5], 'FontSize', FZ);
set(gca, 'XTickLabel', 0:1:4, 'FontSize', FZ);
xlabel('Number of active CPUs', 'FontSize', FZ);
ylabel('Power (Watts)', 'FontSize', FZ);
box off;
title('(C) Snapdrgaon 800 based Nexus 5 smartphone', 'FontSize', FZ);

fname = 'multicore_power';
print('-depsc', fname);
cmd = sprintf('gs -q -sDEVICE=pdfwrite -dEPSCrop -sOutputFile=%s.pdf -dBATCH -dNOPAUSE %s.eps', fname, fname);
system(cmd);

all: tmc_revision.pdf

tmc_revision.pdf: *.tex paper.bib figures/*.pdf
	pdflatex tmc18_revision
	bibtex tmc18_revision
	pdflatex tmc18_revision
	pdflatex tmc18_revision
	pdflatex tmc18_revision

tmc.pdf: *.tex paper.bib figures/*.pdf
	pdflatex tmc18
	bibtex tmc18
	pdflatex tmc18
	pdflatex tmc18
	pdflatex tmc18

atc18.pdf: *.tex paper.bib figures/*.pdf
	pdflatex atc18
	bibtex atc18
	pdflatex atc18
	pdflatex atc18
	pdflatex atc18

eurosys18.pdf: *.tex paper.bib figures/*.pdf
	pdflatex eurosys18
	bibtex eurosys18
	pdflatex eurosys18
	pdflatex eurosys18
	pdflatex eurosys18

sosp17.pdf: *.tex paper.bib figures/*.pdf
	pdflatex sosp17
	bibtex sosp17
	pdflatex sosp17
	pdflatex sosp17
	pdflatex sosp17

hotos17.pdf: *.tex paper.bib figures/*.pdf
	pdflatex hotos17
	bibtex hotos17
	pdflatex hotos17
	pdflatex hotos17
	pdflatex hotos17

clean:
	-@rm -f *~ *.bak *.out *.aux  *.bbl  *.blg  *.dvi  *.log  *.pdf

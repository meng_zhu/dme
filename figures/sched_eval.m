colormap copper;

FZ = 5;

subplot(4,2,3);
%                  /AppLaunch /AppLaunch+AppInstall /Bbench  /Bbench + AppInstall/
Default		= [ 1          1.0646                1        1.0578];
Interactive	= [ 1.03       1.057                 0.9385   0.974];
Y = Default' * [1 0] + Interactive' * [0 1];
h = bar(Y, 0.6, 'group');
legend(h, 'Baseline', 'Interactivity-aware OS');
set(legend, 'Position',[0.28 0.75 0.01 0.01]);
set(legend, 'Orientation', 'horizontal');
axis([0.5 4.5 0 1.1]);
set(gca, 'FontSize', FZ);
set(gca, 'XTick', 1:1:4);
set(gca, 'XTickLabel', {'Launch', 'Launch+Install', 'Bbench', 'Bbench+Install'});
set(gca, 'YTick', [0.9 1.0 1.1]);
ax = gca;
ax.GridLineStyle = '--';
set(gca, 'YGrid', 'on');
ylabel('Normalized latency');
title('(A) Interactive task response latency');
box off;

subplot(4,2,5);
%                  /AppLaunch /AppLaunch+AppInstall /Bbench  /Bbench + AppInstall/
Default		= [ 1          1.2377                1        1.55];
Interactive	= [ 0.77       1.0036                0.9343   1.355];
Y = Default' * [1 0] + Interactive' * [0 1];
h = bar(Y, 0.6, 'group');
axis([0.5 4.5 0 1.6]);
set(gca, 'FontSize', FZ);
set(gca, 'XTick', 1:1:4);
set(gca, 'XTickLabel', {'Launch', 'Launch+Install', 'Bbench', 'Bbench+Install'});
set(gca, 'YTick', [0.6 0.8 1.0 1.2 1.4 1.6]);
ax = gca;
ax.GridLineStyle = '--';
set(gca, 'YGrid', 'on');
ylabel('Normalized energy');
box off;
title('(B) Total energy cost');

fname = 'sched_eval';
print('-depsc', fname);
cmd = sprintf('gs -q -sDEVICE=pdfwrite -dEPSCrop -sOutputFile=%s.pdf -dBATCH -dNOPAUSE %s.eps', fname, fname);
system(cmd);

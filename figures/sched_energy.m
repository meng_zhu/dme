colormap copper;

FZ = 6;

subplot(2,1,1);
set(gca, 'FontSize', FZ);
%                     /AppLaunch/AppLaunch+AppInstall/Bbench/Bbench + AppInstall/
Default             = [ 1        1.2377               1              1.55];
Interactive			= [ 0.77	 1.0036               0.9343         1.355];
Y = Default' * [1 0] + Interactive' * [0 1];
h = bar(Y, 0.6, 'group');
legend(h, 'System default', 'Interactivity context aware scheduling');
set(legend, 'Position',[0.50 0.80 0.01 0.01]);
set(legend, 'Location', 'northoutside');
set(legend, 'Orientation', 'horizontal');
axis([0 5 0 1.6]);
set(gca, 'XTick', 1:1:4);
set(gca, 'XTickLabel', {'AppLaunch', 'AppLaunch+AppInstall', 'Bbench', 'Bbench+AppInstall'});
set(gca, 'YTick', [0.6 0.8 1.0 1.2 1.4 1.6]);
ax = gca;
ax.GridLineStyle = '--';
set(gca, 'YGrid', 'on');
ylabel('Normalized energy cost');
box off;

fname = 'sched_energy';
print('-depsc', fname);
cmd = sprintf('gs -q -sDEVICE=pdfwrite -dEPSCrop -sOutputFile=%s.pdf -dBATCH -dNOPAUSE %s.eps', fname, fname);
system(cmd);

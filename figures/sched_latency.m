colormap copper;

FZ = 6;

subplot(2,1,1);
set(gca, 'FontSize', FZ);
%                     /AppLaunch/AppLaunch+AppInstall/Bbench/Bbench + AppInstall/
Default             = [1        1.0646              1              1.0578];
Interactive			= [1.03     1.057               0.9385		   0.974];
Y = Default' * [1 0] + Interactive' * [0 1];
h = bar(Y, 0.6, 'group');
legend(h, 'System default', 'Interactivity context aware scheduling');
set(legend, 'Position',[0.50 0.80 0.01 0.01]);
set(legend, 'Location', 'northoutside');
set(legend, 'Orientation', 'horizontal');
axis([0 5 0 1.1]);
set(gca, 'XTick', 1:1:4);
set(gca, 'XTickLabel', {'AppLaunch', 'AppLaunch+AppInstall', 'Bbench', 'Bbench+AppInstall'});
set(gca, 'YTick', [0.9 1.0 1.1]);
ax = gca;
ax.GridLineStyle = '--';
set(gca, 'YGrid', 'on');
ylabel('Normalized response latency');
box off;

fname = 'sched_latency';
print('-depsc', fname);
cmd = sprintf('gs -q -sDEVICE=pdfwrite -dEPSCrop -sOutputFile=%s.pdf -dBATCH -dNOPAUSE %s.eps', fname, fname);
system(cmd);

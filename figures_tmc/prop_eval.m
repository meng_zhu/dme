colormap copper;

FZ = 6;

% Ignore -- propagation through only semantic dependencies
% Blind -- propagation through all dependencies

%energy

subplot(4,1,1);
Default    = [ 1 1 1 1 1 1 1 1 1 1 1 1 1];
Ignore  = [ 0.81 0.82 0.87 0.81 0.82 0.89 0.84 0.73 0.87 0.95 0.93 0.86 0.85];
Blind  = [ 0.96 0.91 0.90 0.95 0.96 0.99 0.96 0.91 0.97 0.95 0.94 0.96 0.95];
Uncertain	= [ 0.84 0.84 0.90 0.82 0.84 0.92 0.87 0.78 0.92 0.91 0.91 0.86 0.87];

Y = Default' * [1 0 0 0] + Ignore' * [0 1 0 0] + Blind' * [0 0 1 0] + Uncertain' * [0 0 0 1];
h = bar(Y, 0.6, 'group');
legend(h, ['Default' char(10) 'system'], ['Interactivity propagation through' char(10) 'only higher level dependencies'], ['Interactivity propagation through' char(10) 'all dependencies'],['Interactivity propagation with ' char(10) 'uncertain context']);
set(legend, 'Position',[0.5 0.97 0 0], 'Orientation','horizontal');
axis([0 13.5 0.7 1]);
set(gca, 'FontSize', FZ);
%set(gca, 'XTick', 1:1:4);
set(gca, 'XTickLabel', {'1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', 'Geometric mean'});
set(gca, 'YTick', [0.7 0.8 0.9 1.0]);
ax = gca;
ax.GridLineStyle = '--';
set(gca, 'YGrid', 'on');
ylabel('Normalized energy');
box off;
title('(A) Total active energy consumption');

%interactivity
subplot(4,1,2);
Default    = [ 1 1 1 1 1 1 1 1 1 1 1 1 1];
Ignore  = [ 0.99 1.01 1.02 1.09 1.07 0.94 1.01 1.16 1.00 0.96 0.95 1.16 1.03 ];
Blind  = [ 0.95 1.01 1.03 1.12 0.98 1.03 1.03 1.14 0.99 0.97 1.00 1.13 1.03 ];
Uncertain	= [ 1.02 1.00 1.01 1.06 1.00 0.96 1.02 1.11 0.98 0.96 1.02 1.13 1.02];

Y = Default' * [1 0 0 0] + Ignore' * [0 1 0 0] + Blind' * [0 0 1 0] + Uncertain' * [0 0 0 1];
h = bar(Y, 0.6, 'group');
axis([0 13.5 0.9 1.2]);
set(gca, 'FontSize', FZ);
%set(gca, 'XTick', 1:1:4);
set(gca, 'XTickLabel', {'1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', 'Geometric mean'});
set(gca, 'YTick', [0.9 1 1.1 1.2]);
ax = gca;
ax.GridLineStyle = '--';
set(gca, 'YGrid', 'on');
ylabel('Normalized latency');
title('(B) Interactive task response latency');
box off;

fname = 'prop_eval';
print('-depsc', fname);
%cmd = sprintf('gs -q -sDEVICE=pdfwrite -dEPSCrop -sOutputFile=%s.pdf -dBATCH -dNOPAUSE %s.eps', fname, fname);
cmd = sprintf('gs -q -sDEVICE=pdfwrite -sOutputFile=%s.pdf -dBATCH -dNOPAUSE %s.eps', fname, fname);
system(cmd);

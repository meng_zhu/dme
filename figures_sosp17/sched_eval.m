colormap copper;

FZ = 5;

subplot(4,1,1);
Default    = [ 1 1 1 1 1 1 1 1 1 1 1 1 1];
WithFutex  = [ 0.85 0.98 0.90 0.74 0.93 0.93 0.94 0.70 0.89 0.77 0.74 0.74 0.84];
WithoutFutex  = [ 0.81 0.96 0.90 0.74 0.95 0.94 0.96 0.68 0.77 0.84 0.88 0.78 0.85];
Uncertain	= [ 0.85 0.96 0.90 0.76 0.96 0.90 0.94 0.69 0.89 0.89 0.83 0.82 0.86];

Y = Default' * [1 0 0 0] + WithFutex' * [0 1 0 0] + WithoutFutex' * [0 0 1 0] + Uncertain' * [0 0 0 1];
h = bar(Y, 0.6, 'group');
legend(h, ['Default' char(10) 'system'], ['Interactivity-aware with' char(10) 'futex propagation'], ['Interactivity-aware without' char(10) 'futex propagation'],['Interactivity-aware with ' char(10) 'uncertain context']);
set(legend, 'Position',[0.5 0.97 0 0], 'Orientation','horizontal');
axis([0 13.5 0 1]);
set(gca, 'FontSize', FZ);
%set(gca, 'XTick', 1:1:4);
set(gca, 'XTickLabel', {'1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', 'Average'});
set(gca, 'YTick', [0.6 0.7 0.8 0.9 1.0]);
ax = gca;
ax.GridLineStyle = '--';
set(gca, 'YGrid', 'on');
ylabel('Normalized energy');
box off;
title('(A) Total active energy consumption');

subplot(4,1,2);
Default    = [ 1 1 1 1 1 1 1 1 1 1 1 1 1];
WithFutex  = [1.16 0.90 1.07 1.11 1.12 1.10 1.12 1.21 1.14 1.17 1.14 1.33 1.13];
WithoutFutex  = [1.12 0.95 1.11 1.07 1.10 1.02 1.15 1.12 1.12 1.16 1.11 1.18 1.10];
Uncertain	= [1.04 0.86 1.03 1.03 1.01 1.03 1.03 1.08 1.06 1.04 1.06 1.06 1.03];

Y = Default' * [1 0 0 0] + WithFutex' * [0 1 0 0] + WithoutFutex' * [0 0 1 0] + Uncertain' * [0 0 0 1];
h = bar(Y, 0.6, 'group');
axis([0 13.5 0 1.3]);
set(gca, 'FontSize', FZ);
%set(gca, 'XTick', 1:1:4);
set(gca, 'XTickLabel', {'1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', 'Average'});
set(gca, 'YTick', [1 1.1 1.2 1.3]);
ax = gca;
ax.GridLineStyle = '--';
set(gca, 'YGrid', 'on');
ylabel('Normalized latency');
title('(B) Interactive task response latency');
box off;























fname = 'sched_eval';
print('-depsc', fname);
cmd = sprintf('gs -q -sDEVICE=pdfwrite -dEPSCrop -sOutputFile=%s.pdf -dBATCH -dNOPAUSE %s.eps', fname, fname);
system(cmd);

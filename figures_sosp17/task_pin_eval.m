colormap copper;

FZ = 5;

subplot(4,1,1);

% energy

Default    = [ 1 1 1 1 1 1 1 1 1 1 1 1 1];
No_pin = [ 0.85 0.96 0.90 0.76 0.96 0.90 0.94 0.69 0.89 0.89 0.83 0.82 0.86 ];
Task_pin = [ 0.80 0.93 0.88 0.71 0.91 0.84 0.87 0.66 0.85 0.85 0.81 0.79 0.82 ];

Y = Default' * [1 0 0] + No_pin' * [0 1 0] + Task_pin' * [0 0 1];
h = bar(Y, 0.6, 'group');
legend(h, ['Default' char(10) 'system'], ['Interactivity-aware scheduling' char(10) 'without background task consolidation'], ['Interactivity-aware scheduling' char(10) 'with background task consolidation']);

set(legend, 'Position',[0.5 0.97 0 0], 'Orientation','horizontal');
axis([0 13.5 0 1]);
set(gca, 'FontSize', FZ);
%set(gca, 'XTick', 1:1:4);
set(gca, 'XTickLabel', {'1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', 'Average'});
set(gca, 'YTick', [0.6 0.7 0.8 0.9 1.0]);
ax = gca;
ax.GridLineStyle = '--';
set(gca, 'YGrid', 'on');
ylabel('Normalized energy');
box off;
title('(A) Total active energy consumption');

subplot(4,1,2);

%interactivity 

Default    = [ 1 1 1 1 1 1 1 1 1 1 1 1 1];
No_pin = [ 1.04 0.86 1.03 1.03 1.01 1.03 1.03 1.08 1.06 1.04 1.06 1.06 1.03 ];
Task_pin = [ 1.08 0.94 0.98 1.07 1.03 1.03 1.07 1.05 1.06 1.05 1.04 1.02 1.04 ];

Y = Default' * [1 0 0] + No_pin' * [0 1 0] + Task_pin' * [0 0 1];
h = bar(Y, 0.6, 'group');
axis([0 13.5 0 1.2]);
set(gca, 'FontSize', FZ);
%set(gca, 'XTick', 1:1:4);
set(gca, 'XTickLabel', {'1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', 'Average'});
set(gca, 'YTick', [1 1.1]);
ax = gca;
ax.GridLineStyle = '--';
set(gca, 'YGrid', 'on');
ylabel('Normalized latency');
title('(B) Interactive task response latency');
box off;

fname = 'task_pin_eval';
print('-depsc', fname);
cmd = sprintf('gs -q -sDEVICE=pdfwrite -dEPSCrop -sOutputFile=%s.pdf -dBATCH -dNOPAUSE %s.eps', fname, fname);
system(cmd);
